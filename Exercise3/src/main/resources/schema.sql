CREATE TABLE IF NOT EXISTS exchange_rates (
 id integer not null auto_increment,
 symbol varchar(3),
 rate decimal(10,6),
 exchange_date varchar(10),
 primary key(id)
);