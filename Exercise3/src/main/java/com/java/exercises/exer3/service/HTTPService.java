package com.java.exercises.exer3.service;

import com.java.exercises.exer3.model.RateAPIModel;

/**
 * @author b.orobia
 *
 */
public interface HTTPService {

	RateAPIModel get(String url);
}
