package com.java.exercises.exer3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.java.exercises.exer3.service.ExchangeRateService;

/**
 * @author b.orobia
 *
 */
@SpringBootApplication
public class Exercise3Application {

	@Autowired
	private ExchangeRateService serv;

	public static void main(String[] args) {
		SpringApplication.run(Exercise3Application.class, args);
	}

	public void run(String args) throws Exception {

		serv.fetchExchangeRates("USD,AUD,JEP");
	}

}
