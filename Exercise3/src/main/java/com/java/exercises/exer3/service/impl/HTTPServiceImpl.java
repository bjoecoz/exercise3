package com.java.exercises.exer3.service.impl;

import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.exercises.exer3.model.RateAPIModel;
import com.java.exercises.exer3.service.HTTPService;

/**
 * @author b.orobia
 *
 */
@Service
public class HTTPServiceImpl implements HTTPService{

	private static final CloseableHttpClient httpclient = HttpClients.createDefault();
	
	@Override
	public RateAPIModel get(String url) {

		
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("Accept", "application/json");
		httpget.setHeader("Content-type", "application/json");
		
		try (CloseableHttpResponse response = httpclient.execute(httpget)) {

			if (response.getStatusLine().getStatusCode() == 200) {
				try {

					return new ObjectMapper().readValue(
							response.getEntity().getContent(), RateAPIModel.class);

				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
