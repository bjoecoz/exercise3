package com.java.exercises.exer3.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.java.exercises.exer3.entity.RateEntity;

/**
 * @author b.orobia
 *
 */
@Mapper
public interface ExchangeRateMapper {


	@Select("SELECT EXISTS(SELECT 1 FROM exchange_rates WHERE symbol = #{symbol} and exchange_date= #{exchangeDate})")
	boolean isRateExist(RateEntity entity);

	@Insert("INSERT INTO exchange_rates (symbol, rate, exchange_date) "
			+ "VALUES (#{symbol}, #{rate}, #{exchangeDate})")
	void insert(RateEntity apiData);

	@Select("SELECT * FROM exchange_rates")
	List<RateEntity> selectAll();
	
	@Select("SELECT * FROM exchange_rates WHERE exchange_date= #{exchangeDate}")
	List<RateEntity> selectAllByDate(String exchangeDate);
	
	@Select("SELECT * FROM exchange_rates WHERE symbol = #{symbol}")
	List<RateEntity> selectBySymbol(String symbol);
	
	@Select("SELECT * FROM exchange_rates WHERE symbol = #{symbol} and exchange_date= #{exchangeDate}")
	RateEntity selectBySymbolAndDate(String symbol, String exchangeDate);
}
