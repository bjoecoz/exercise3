package com.java.exercises.exer3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.exercises.exer3.service.ExchangeRateService;

/**
 * @author b.orobia
 *
 */
@RestController
@RequestMapping("/api/v1/exchange-rate")
public class ExchangeRateController {
	
	@Autowired
	private ExchangeRateService erService;
	
	@GetMapping("/fetch")
	public ResponseEntity<Object> fetchRateLatest(
			@RequestParam("symbol") String symbol) {
		
		erService.fetchExchangeRates(symbol);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/fetch/{date}")
	public ResponseEntity<Object> fetchRateByDate(
			@PathVariable("date") String date,
			@RequestParam("symbol") String symbol) {
		
		erService.fetchExchangeRates(date, symbol);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/all")
	public ResponseEntity<Object> retrieveAll() {
		
		return ResponseEntity.ok(erService.retrieveAll());
	}
	
	@GetMapping("/all/{date}")
	public ResponseEntity<Object> retrieveAllByDate(
			@PathVariable("date") String date) {
		
		return ResponseEntity.ok(erService.retrieveAll(date));
	}
	
	@GetMapping("/{symbol}")
	public ResponseEntity<Object> retrieveBySymbol(
			@PathVariable("symbol") String symbol) {
		
		return ResponseEntity.ok(erService.retrieveBySymbol(symbol));
	}
	
	@GetMapping("/{symbol}/{date}")
	public ResponseEntity<Object> retrieveBySymbolAndDate(
			@PathVariable("symbol") String symbol,
			@PathVariable("date") String date) {
		
		return ResponseEntity.ok(erService.retrieveBySymbolAndDate(symbol, date));
	}

}
