package com.java.exercises.exer3.entity;

/**
 * @author b.orobia
 *
 */
public class RateEntity {

	private int id;
	private String symbol;
	private double rate;
	private String exchangeDate;

	public RateEntity() {}
	
	public RateEntity(String symbol, double	rate, String date) {
		setSymbol(symbol);
		setRate(rate);
		setExchangeDate(date);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getExchangeDate() {
		return exchangeDate;
	}

	public void setExchangeDate(String exchangeDate) {
		this.exchangeDate = exchangeDate;
	}

	@Override
	public String toString() {
		return "RateEntity [id=" + id + ", symbol=" + symbol + ", rate=" + rate + ", exchangeDate=" + exchangeDate
				+ "]";
	}
	
	
	
}
