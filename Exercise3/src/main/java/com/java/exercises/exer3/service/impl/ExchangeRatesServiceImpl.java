package com.java.exercises.exer3.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer3.entity.RateEntity;
import com.java.exercises.exer3.mapper.ExchangeRateMapper;
import com.java.exercises.exer3.model.RateAPIModel;
import com.java.exercises.exer3.service.ExchangeRateService;
import com.java.exercises.exer3.service.HTTPService;

/**
 * @author b.orobia
 *
 */
@Service
public class ExchangeRatesServiceImpl implements ExchangeRateService {

	private static final String EXCHANGE_RATE_ABASE_API = "http://data.fixer.io/api/";
	private static final String EXCHANGE_RATE_ACCESS_KEY = "750447aa7846a07751c774adc14403ab";

	// TODO: SIMPLE REGEX PATTERN ONLY, NEEDS TO CHANGE TO A MORE ACCURATE PATTERN
	// YYYY-MM-DD
	private static final String DATE_PATTERN = "\\d{4}-\\d{2}-\\d{2}";

	DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Autowired
	private HTTPService httpService;

	@Autowired
	private ExchangeRateMapper erMapper;

	@Override
	public List<RateEntity> retrieveBySymbol(String symbol) {

		return erMapper.selectBySymbol(symbol);
	}

	@Override
	public RateEntity retrieveBySymbolAndDate(String symbol, String date) {

		if (!date.matches(DATE_PATTERN))
			throw new IllegalArgumentException("Error: Invalid date pattern passed. Should be (yyyy-mm-dd)");

		return erMapper.selectBySymbolAndDate(symbol, date);
	}

	@Override
	public List<RateEntity> retrieveAll() {

		return erMapper.selectAll();
	}

	@Override
	public List<RateEntity> retrieveAll(String date) {

		if (!date.matches(DATE_PATTERN))
			throw new IllegalArgumentException("Error: Invalid date pattern passed. Should be (yyyy-mm-dd)");

		LocalDate parsedDate = LocalDate.parse(date, formatters);

		return erMapper.selectAllByDate(parsedDate.toString());
	}

	@Override
	public RateAPIModel fetchExchangeRates(String symbols) {

//		String strSymbol = String.join(",", symbols);

		String url = EXCHANGE_RATE_ABASE_API + "latest?access_key=" + EXCHANGE_RATE_ACCESS_KEY + "&symbols="
				+ symbols;

		RateAPIModel apiData = httpService.get(url);
		insertDataToDB(apiData);

		return apiData;
	}

	@Override
	public RateAPIModel fetchExchangeRates(String date, String symbols) {

		if (!date.matches(DATE_PATTERN))
			throw new IllegalArgumentException("Error: Invalid date pattern passed. Should be (yyyy-mm-dd)");

//		String strSymbol = String.join(",", symbols);

		String url = EXCHANGE_RATE_ABASE_API + date + "?access_key=" + EXCHANGE_RATE_ACCESS_KEY + "&symbols="
				+ symbols;

		RateAPIModel apiData = httpService.get(url);
		insertDataToDB(apiData);

		return apiData;
	}

	private void insertDataToDB(RateAPIModel apiData) {

		if (apiData == null)
			throw new RuntimeException("Invalid argument passed in method insertDataToDB().");

		for (Entry<String, Double> rates : apiData.getRates().entrySet()) {
			RateEntity entity = new RateEntity(rates.getKey(), rates.getValue(), apiData.getDate());

			if (!erMapper.isRateExist(entity))
				erMapper.insert(entity);
		}
	}

}
