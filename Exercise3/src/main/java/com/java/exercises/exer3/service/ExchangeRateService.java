package com.java.exercises.exer3.service;

import java.util.List;

import com.java.exercises.exer3.entity.RateEntity;
import com.java.exercises.exer3.model.RateAPIModel;

/**
 * @author b.orobia
 *
 */
public interface ExchangeRateService {

	/**
	 * Fetch Latest Exchange Rate of the given array of symbols.
	 * 
	 * @param symbol
	 * @return
	 */
	RateAPIModel fetchExchangeRates(String symbols);
	
	/**
	 * Fetch Latest Exchange Rate of the given date and array of symbols.
	 *
	 * @param date (YYY-MM-DD)
	 * @param symbol 
	 * @return
	 */
	RateAPIModel fetchExchangeRates(String date, String symbols);
	
	/**
	 * Retrieve Latest Exchange Rate of the given symbol.
	 * @param symbol
	 * @return
	 */
	List<RateEntity> retrieveBySymbol(String symbol);
	
	/**
	 * Retrieve Exchange Rate of the given symbol and date.
	 * @param symbol
	 * @param date (YYY-MM-DD)
	 * @return
	 */
	RateEntity retrieveBySymbolAndDate(String symbol, String date);
	
	/**
	 * Retrieve Latest Exchange Rate of all symbols. 
	 * @return
	 */
	List<RateEntity> retrieveAll();
	
	/**
	 * Retrieve Exchange Rate of all symbols of the given date.
	 * @param date (YYY-MM-DD)
	 * @return
	 */
	List<RateEntity> retrieveAll(String date);
}
