package com.java.exercises.exer3.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.java.exercises.exer3.entity.RateEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test_retrieveAll {

	@Autowired
	private ExchangeRateService service;
	
	@Test
	public void test_retrieveAllLatest() {

		List<RateEntity> actual = service.retrieveAll();
		
		assertNotNull(actual);
		actual.forEach(a -> System.out.println(a));
	}
	
	@Test
	public void test_retrieveAllByDate() {

		String date = "2020-10-28";
		
		List<RateEntity> actual = service.retrieveAll(date);
		
		assertNotNull(actual);
		actual.forEach(a -> System.out.println(a));
		
	}

}
