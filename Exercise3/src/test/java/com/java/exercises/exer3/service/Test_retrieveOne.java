package com.java.exercises.exer3.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.java.exercises.exer3.entity.RateEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test_retrieveOne {

	@Autowired
	private ExchangeRateService service;
	
	@Test
	public void test_retrieveOneLatest() {

		List<RateEntity> actual = service.retrieveBySymbol("USD");
		
		assertNotNull(actual);
	}
	
	@Test
	public void test_retrieveOneByDate() {

		String date = "2020-10-28";
		
		RateEntity actual = service.retrieveBySymbolAndDate("USD", date);
		
		assertNotNull(actual);
		
	}

}
