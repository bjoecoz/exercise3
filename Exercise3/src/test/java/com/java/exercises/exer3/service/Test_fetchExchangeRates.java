package com.java.exercises.exer3.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.java.exercises.exer3.model.RateAPIModel;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test_fetchExchangeRates {

	@Autowired
	private ExchangeRateService service;
	
	@Test
	public void test_FetchLatestExchangeRate() {

		String symbols = "USD,JEP,AUD";
		RateAPIModel actual = service.fetchExchangeRates(symbols);
		
		assertTrue(actual.isSuccess());
		assertFalse(actual.isHistorical());
		assertEquals("EUR", actual.getBase());
		assertEquals(3, actual.getRates().size());
		
		assertTrue(actual.getRates().keySet().contains("USD"));
		assertTrue(actual.getRates().keySet().contains("JEP"));
		assertTrue(actual.getRates().keySet().contains("AUD"));
		
	}
	
	@Test
	public void test_FecthExchangeRateByDate() {

		String symbols = "USD,JEP,AUD";
		String date = "2020-10-28";
		
		RateAPIModel actual = service.fetchExchangeRates(date, symbols);
		
		assertTrue(actual.isSuccess());
		assertTrue(actual.isHistorical());
		assertEquals("EUR", actual.getBase());
		assertEquals(3, actual.getRates().size());
		
		assertTrue(actual.getRates().keySet().contains("USD"));
		assertTrue(actual.getRates().keySet().contains("JEP"));
		assertTrue(actual.getRates().keySet().contains("AUD"));
		
	}

}
